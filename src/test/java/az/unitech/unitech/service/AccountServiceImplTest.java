package az.unitech.unitech.service;

import az.unitech.unitech.entity.Account;
import az.unitech.unitech.entity.User;
import az.unitech.unitech.enums.Status;
import az.unitech.unitech.exception.DataAlreadyExistsException;
import az.unitech.unitech.lib.Data;
import az.unitech.unitech.repository.AccountRepository;
import az.unitech.unitech.request.AccountRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    private AccountService accountService;
    private Data data;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    UserService userService;

    @BeforeEach
    void setUp() {
        accountService = new AccountServiceImpl(accountRepository, userService, data);
    }

    User user = User.builder()
            .id(1L)
            .pin("1234")
            .password("pass")
            .build();
    Account account1 = Account.builder()
            .name("AA")
            .balance(200.00)
            .status(Status.ACTIVE)
            .user(user)
            .build();
    Account account2 = Account.builder()
            .name("AB")
            .balance(100.00)
            .status(Status.ACTIVE)
            .user(user)
            .build();

    @Test
    void whenAccountExistsThenThrowDataExistsException() {
        AccountRequest request = AccountRequest.builder()
                .name("AA")
                .status(Status.ACTIVE)
                .balance(200.00)
                .userId(1L)
                .build();
        when(userService.findById(1L)).thenReturn(user);
        when(accountRepository.findAccountByName("AA")).thenReturn(account1);

        assertThatThrownBy(()->accountService.createAccount(request)).isInstanceOf(DataAlreadyExistsException.class);
    }

    @Test
    void whenAccountNotExistsThenCreateAccount() {
        AccountRequest request = AccountRequest.builder()
                .name("AA")
                .status(Status.ACTIVE)
                .balance(200.00)
                .userId(1L)
                .build();
        when(userService.findById(1L)).thenReturn(user);
        when(accountRepository.findAccountByName("AA")).thenReturn(null);
        when(accountRepository.save(account1)).thenReturn(account1);

        accountService.createAccount(request);

        assertThat(account1.getName()).isEqualTo("AA");
        assertThat(account1.getBalance()).isEqualTo(200.00);
        assertThat(account1.getUser()).isEqualTo(user);
    }

    @Test
    void whenUserHaveActiveAccountThenGetAll() {

        List<Account> accountList = List.of(account1, account2);

        when(userService.findById(1L)).thenReturn(user);
        when(accountRepository.findAccountByUserId(1L)).thenReturn(accountList);

        accountService.getAllActiveAccount(1L);

        assertThat(accountList.get(0).getBalance()).isEqualTo(200.00);
        assertThat(accountList.get(0).getName()).isEqualTo("AA");
        assertThat(accountList.get(0).getStatus()).isEqualTo(Status.ACTIVE);
        assertThat(accountList.get(0).getUser()).isEqualTo(user);

        assertThat(accountList.get(1).getBalance()).isEqualTo(100.00);
        assertThat(accountList.get(1).getName()).isEqualTo("AB");
        assertThat(accountList.get(1).getStatus()).isEqualTo(Status.ACTIVE);
        assertThat(accountList.get(1).getUser()).isEqualTo(user);
    }

    @Test
    void whenDataIsCorrectThenTransferStart() {

        when(accountRepository.findAccountByName("AA")).thenReturn(account1);
        when(accountRepository.findAccountByName("AB")).thenReturn(account2);

        accountService.transferAccountToAccount("AA", "AB", 50.00);

        assertThat(account1.getBalance()).isEqualTo(150.00);
        assertThat(account2.getBalance()).isEqualTo(150.00);
    }

}