package az.unitech.unitech.service;

import az.unitech.unitech.entity.User;
import az.unitech.unitech.exception.DataAlreadyExistsException;
import az.unitech.unitech.exception.InvalidDataException;
import az.unitech.unitech.repository.UserRepository;
import az.unitech.unitech.request.UserRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImpl(userRepository);
    }

    UserRequest request = UserRequest.builder()
            .pin("1234")
            .password("pass")
            .build();

    User user = User.builder()
            .pin("1234")
            .password("pass")
            .build();

    @Test
    void whenPinNotExsistThenReturnSuccessRegister() {

        when(userRepository.findByPin("1234")).thenReturn(null);
        when(userRepository.save(user)).thenReturn(user);

        userService.createUser(request);

        assertThat(user.getPin()).isEqualTo("1234");
        assertThat(user.getPassword()).isEqualTo("pass");
    }

    @Test
    void whenPinExsistThenReturnPinAlreadyExistsException() {

        when(userRepository.findByPin("1234")).thenReturn(user);

        assertThatThrownBy(()->userService.createUser(request)).isInstanceOf(DataAlreadyExistsException.class)
                .hasMessage("User already registered : "+ request.getPin() + " pin");
    }

    @Test
    void whenUserDataIsRightThenUserSuccessfullyLogin() {

        when(userRepository.findByPinAndPassword("1234", "pass")).thenReturn(Optional.of(user));

        userService.loginUser(request);

        assertThat(user.getPin()).isEqualTo("1234");
        assertThat(user.getPassword()).isEqualTo("pass");
    }

    @Test
    void whenUserDataIsWrongThenInvalidDataException() {

        when(userRepository.findByPinAndPassword("1234", "pass")).thenReturn(Optional.empty());

        assertThatThrownBy(()->userService.loginUser(request)).isInstanceOf(InvalidDataException.class)
                .hasMessage("Pin or Password is wrong ");
    }
}