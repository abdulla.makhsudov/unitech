package az.unitech.unitech.enums;

public enum Status {
    ACTIVE, INACTIVE
}