package az.unitech.unitech.controller;

import az.unitech.unitech.request.AccountRequest;
import az.unitech.unitech.response.AccountResponse;
import az.unitech.unitech.response.ApiResponse;
import az.unitech.unitech.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping("/account")
    public ResponseEntity<ApiResponse> createAccount(@RequestBody AccountRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(accountService.createAccount(request));
    }

    @GetMapping("/account/{id}")
    public ResponseEntity<List<AccountResponse>> getAllActiveAccount(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(accountService.getAllActiveAccount(id));
    }

    @PostMapping("/account/transfer")
    public ResponseEntity<ApiResponse> transferAccountToAccount(String sourceAccount, String targetAccount, Double amount) {
        return ResponseEntity.status(HttpStatus.OK).body(accountService.transferAccountToAccount(sourceAccount, targetAccount, amount));
    }

    @GetMapping("/account/currency-rate/")
    public ResponseEntity<Map<String, Double>> getCurrencyRate(@RequestParam String currencyPair) {
        return ResponseEntity.ok(accountService.getCurrencyRate(currencyPair));
    }


}
