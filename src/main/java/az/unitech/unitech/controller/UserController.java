package az.unitech.unitech.controller;

import az.unitech.unitech.request.UserRequest;
import az.unitech.unitech.response.ApiResponse;
import az.unitech.unitech.response.UserResponse;
import az.unitech.unitech.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity<ApiResponse<UserResponse>> createUser(@RequestBody UserRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(request));
    }

    @PostMapping("/login")
    public ResponseEntity<ApiResponse<UserResponse>> loginUser(@RequestBody UserRequest request) {
        return  ResponseEntity.status(HttpStatus.OK).body(userService.loginUser(request));
    }
}
