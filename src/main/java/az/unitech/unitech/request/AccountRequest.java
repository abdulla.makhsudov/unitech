package az.unitech.unitech.request;

import az.unitech.unitech.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountRequest {

    private Long userId;
    private String name;
    private Double balance;
    private Status status;

}
