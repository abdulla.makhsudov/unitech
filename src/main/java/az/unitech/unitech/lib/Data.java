package az.unitech.unitech.lib;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class Data {

    Map<String, Double> currencyRate = Map.of(
            "USD/AZN", 1.7,
            "AZN/TL", 8.0,
            "EUR/AZN", 1.8,
            "USD/TL", 17.25,
            "USD/EUR", 0.95
    );

    public Map<String, Double> getCurrencyRate() {
        return currencyRate;
    }
}
