package az.unitech.unitech.exception;

import az.unitech.unitech.response.ApiResponse;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class RestExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ApiResponse> notFoundException(NotFoundException ex) {
        return new ResponseEntity<>(generalApiResponse(ex, 404), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DataAlreadyExistsException.class)
    public ResponseEntity<ApiResponse> dataAlreadyExists(DataAlreadyExistsException ex) {
        return new ResponseEntity<>(generalApiResponse(ex, 409), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(InvalidDataException.class)
    public ResponseEntity<ApiResponse> invalidDataException(InvalidDataException ex) {
        return new ResponseEntity<>(generalApiResponse(ex, 400), HttpStatus.BAD_REQUEST);
    }

    public ApiResponse generalApiResponse(Exception ex, int statusCode) {
        return ApiResponse.builder()
                .message(ex.getMessage())
                .creationalTime(LocalDateTime.now())
                .statusCode(statusCode)
                .build();
    }

}
