package az.unitech.unitech.service;

import az.unitech.unitech.request.AccountRequest;
import az.unitech.unitech.response.AccountResponse;
import az.unitech.unitech.response.ApiResponse;

import java.util.List;
import java.util.Map;

public interface AccountService {

    ApiResponse<AccountResponse> createAccount(AccountRequest request);

    List<AccountResponse> getAllActiveAccount(Long id);

    ApiResponse transferAccountToAccount(String sourceAccount, String targetAccount, Double amount);

    Map<String, Double> getCurrencyRate(String currencyPair);
}
