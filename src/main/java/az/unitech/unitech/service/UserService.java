package az.unitech.unitech.service;

import az.unitech.unitech.entity.User;
import az.unitech.unitech.request.UserRequest;
import az.unitech.unitech.response.ApiResponse;
import az.unitech.unitech.response.UserResponse;

public interface UserService {

    ApiResponse<UserResponse> createUser(UserRequest request);

    ApiResponse<UserResponse> loginUser(UserRequest request);

    User findById(Long userId);
}
