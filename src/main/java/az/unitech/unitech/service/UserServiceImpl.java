package az.unitech.unitech.service;

import az.unitech.unitech.entity.User;
import az.unitech.unitech.exception.DataAlreadyExistsException;
import az.unitech.unitech.exception.InvalidDataException;
import az.unitech.unitech.repository.UserRepository;
import az.unitech.unitech.request.UserRequest;
import az.unitech.unitech.response.ApiResponse;
import az.unitech.unitech.response.UserResponse;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public ApiResponse<UserResponse> createUser(UserRequest request) {
        if (userRepository.findByPin(request.getPin()) != null) {
            throw new DataAlreadyExistsException("User already registered : "+ request.getPin() + " pin");
        }
        User user = User.builder()
                .pin(request.getPin())
                .password(request.getPassword())
                .build();
        User saveUser = userRepository.save(user);
        UserResponse userResponse = UserResponse.builder().pin(saveUser.getPin()).id(saveUser.getId()).build();
        return ApiResponse.<UserResponse>builder()
                .creationalTime(LocalDateTime.now())
                .message("successfully created")
                .statusCode(201)
                .data(userResponse)
                .build();
    }

    @Override
    public ApiResponse<UserResponse> loginUser(UserRequest request) {
        User user = userRepository.findByPinAndPassword(request.getPin(), request.getPassword()).orElseThrow(() ->
                new InvalidDataException("Pin or Password is wrong "));
        UserResponse userResponse = UserResponse.builder().pin(user.getPin()).id(user.getId()).build();
        return ApiResponse.<UserResponse>builder()
                .creationalTime(LocalDateTime.now())
                .message("successfully entry")
                .statusCode(200)
                .data(userResponse)
                .build();
    }

    @SneakyThrows
    @Override
    public User findById(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new NotFoundException("User : " +userId+ " not found"));
    }
}
