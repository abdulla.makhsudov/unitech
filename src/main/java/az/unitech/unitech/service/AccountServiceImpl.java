package az.unitech.unitech.service;

import az.unitech.unitech.entity.Account;
import az.unitech.unitech.entity.User;
import az.unitech.unitech.enums.Status;
import az.unitech.unitech.exception.DataAlreadyExistsException;
import az.unitech.unitech.exception.InvalidDataException;
import az.unitech.unitech.lib.Data;
import az.unitech.unitech.repository.AccountRepository;
import az.unitech.unitech.request.AccountRequest;
import az.unitech.unitech.response.AccountResponse;
import az.unitech.unitech.response.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final UserService userService;
    private final Data data;
    private Map<String, Double> currencyRate;

    @Override
    public ApiResponse<AccountResponse> createAccount(AccountRequest request) {
        User user = userService.findById(request.getUserId());
        if (accountRepository.findAccountByName(request.getName()) != null) {
            throw new DataAlreadyExistsException("Account already exists : " + request.getName() + " name");
        }

        Account account = Account.builder()
                .name(request.getName())
                .balance(request.getBalance())
                .status(request.getStatus())
                .user(user)
                .build();
        Account save = accountRepository.save(account);
        AccountResponse accountResponse = AccountResponse.builder()
                .id(save.getId())
                .name(save.getName())
                .balance(save.getBalance())
                .status(save.getStatus())
                .build();
        return ApiResponse.<AccountResponse>builder()
                .creationalTime(LocalDateTime.now())
                .message("successfully created")
                .statusCode(201)
                .data(accountResponse)
                .build();
    }

    @Override
    public List<AccountResponse> getAllActiveAccount(Long id) {
        User user = userService.findById(id);
        return accountRepository.findAccountByUserId(user.getId()).stream().filter(account -> account.getStatus().equals(Status.ACTIVE))
                .map(account -> AccountResponse.builder()
                        .id(account.getId())
                        .name(account.getName())
                        .balance(account.getBalance())
                        .status(account.getStatus())
                        .build()
                ).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public ApiResponse transferAccountToAccount(String sourceAccount, String targetAccount, Double amount) {
        Account sourceAccountRepo = getAccountByStatus(sourceAccount);
        Account targetAccountRepo = getAccountByStatus(targetAccount);
        if (sourceAccountRepo != null && targetAccountRepo != null) {

            isActiveAccount(sourceAccountRepo, targetAccountRepo);
            checkMoney(sourceAccount, targetAccount, amount);
            isSameAccount(sourceAccount, targetAccount);

            sourceAccountRepo.setBalance(sourceAccountRepo.getBalance() - amount);
            targetAccountRepo.setBalance(targetAccountRepo.getBalance() + amount);

        } else {
            throw new InvalidDataException("source or target account non exists");
        }
        return ApiResponse.builder()
                .message("successfully transfer")
                .creationalTime(LocalDateTime.now())
                .statusCode(200)
                .build();
    }

    @Override
    public Map<String, Double> getCurrencyRate(String currencyPair) {
        if (!this.currencyRate.isEmpty()){
            Map<String, Double> currencyRateFromLib = this.currencyRate;
            if (currencyRateFromLib.containsKey(currencyPair)) {
                return currencyRateFromLib.entrySet().stream()
                        .filter(e -> currencyPair.equals(e.getKey()))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            }
            return currencyRateFromLib;
        } else {
            currencyRate = new HashMap<>();
            currencyRate.put("USD/AZN", 1.7);
            return currencyRate;
        }
    }

    @Scheduled(cron = "0 * * * * *")
    public void getCurrencyRateFromLib() {
        currencyRate = data.getCurrencyRate();
    }

    private void isSameAccount(String sourceAccount, String targetAccount) {
        if (sourceAccount.equals(targetAccount))
            throw new InvalidDataException("You don't transfer between same account");
    }

    private void checkMoney(String sourceAccount, String targetAccount, Double amount) {
        if (accountRepository.findAccountByName(sourceAccount).getBalance() < amount)
            throw new InvalidDataException("Your account : " + targetAccount + "  no enough money");
    }

    private void isActiveAccount(Account sourceAccountRepo, Account targetAccountRepo) {
        if (!sourceAccountRepo.getStatus().equals(Status.ACTIVE) || !targetAccountRepo.getStatus().equals(Status.ACTIVE))
            throw new InvalidDataException("source or target account is inactive");
    }

    private Account getAccountByStatus(String account) {
        return accountRepository.findAccountByName(account);
    }
}
