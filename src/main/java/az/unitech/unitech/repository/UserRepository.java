package az.unitech.unitech.repository;


import az.unitech.unitech.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByPin(String pin);

	Optional<User> findByPinAndPassword(String pin, String password);
}
