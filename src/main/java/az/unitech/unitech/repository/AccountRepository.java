package az.unitech.unitech.repository;

import az.unitech.unitech.entity.Account;
import az.unitech.unitech.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    List<Account> findAccountByUserId(Long id);

    Account findAccountByName(String name);

    Account findAccountByNameAndStatus(String name, Status status);

    Account findAccountByStatus(Status status);
}
